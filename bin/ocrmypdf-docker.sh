#!/usr/bin/env bash
#
# Part of paperless.sh
# https://gitlab.com/marcelbrueckner/paperless
#
# This file abstracts the Docker "overhead" to use dockerized OCRmyPDF as if it were native.
# You will still need to have Docker installed.
#
# After the argument substitution performend by this file, running it with e.g. the following arguments
#   ocrmypdf-docker.sh --output pdf --deskew nested/directory/inputfile nested/twice/another_directory/outputfile
# would result in
#   docker run --rm -v nested/directory:/input nested/twice/another_directory:/output --log-driver=none jbarlow83/ocrmypdf --output pdf --deskew /input/inputfile /output/outputfile
#
# Place this file named as `ocrmypdf` into your $PATH.

if [ -z "$PAPERLESS_SH_OCRMYPDF_DOCKER_IMAGE" ]; then
    PAPERLESS_SH_OCRMYPDF_DOCKER_IMAGE=jbarlow83/ocrmypdf
fi

# Parse input and output files and provide parent folders to the container as volumes
# Only suitable if more than one argument passed to script. A single argument only is probably --help
if [ $# -gt 1 ]; then
    # Split path and file names into arrays
    # https://stackoverflow.com/questions/11054939/how-to-get-the-second-last-argument-from-shell-script/5995009#11055032
    # https://github.com/koalaman/shellcheck/wiki/SC2124
    # https://stackoverflow.com/questions/19477236/need-help-in-splitting-filename-and-folder-path-using-shell-script/5995009#19478069
    # https://unix.stackexchange.com/questions/253524/dirname-and-basename-vs-parameter-expansion
    input="${*:(-2):1}"
    input=( "$(dirname -- "$input")" "$(basename -- "$input")" ) # instead of input=("${input%/*}" "${input##/}")
    output="${*:(-1):1}"
    output=( "$(dirname -- "$output")" "$(basename -- "$output")" ) # instead of output=("${output%/*}" "${output##/}")

    if [ "${input[0]}" = "." ]; then
        input[0]=$(pwd)
    fi
    if [ "${output[0]}" = "." ]; then
        output[0]=$(pwd)
    fi
    volumes="-v ${input[0]}:/input -v ${output[0]}:/output"

    # Remove previous file path from arguments and 
    # https://stackoverflow.com/questions/4827690/how-to-change-a-command-line-argument-in-bash/5995009#4827707
    # https://stackoverflow.com/questions/20398499/remove-last-argument-from-argument-list-of-shell-script-bash/5995009#26163980
    set -- "${@:1:$(($#-2))}" "/input/${input[1]}" "/output/${output[1]}"
fi

# Very small performance hack picked up on GitHub
# https://github.com/moby/moby/issues/21346#issuecomment-199060492
if [ -z "$PAPERLESS_SH_DEBUG" ] && [ "$PAPERLESS_SH_DEBUG" != "0" ]; then
    log_driver="--log-driver=none"
fi

# shellcheck disable=SC2086
docker run --rm $volumes $log_driver "$PAPERLESS_SH_OCRMYPDF_DOCKER_IMAGE" "$@"
