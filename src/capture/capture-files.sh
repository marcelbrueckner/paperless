#!/usr/bin/env bash
#
# Part of paperless.sh
# https://gitlab.com/marcelbrueckner/paperless
#
# Capture files matching a pattern from a source to a target directory
#

# Exit on error
set -e

readonly START_DATETIME=$(date +"%Y%m%d%H%M%S%z")

function usage {
  echo "usage: $0 [-i /path/to/sourcedir] [-o /path/to/targetdir] [-p pattern]"
  echo "   "
  echo "  -i | --source-dir        : Source directory that should be searched for files matching the --pattern, defaults to the current working directory"
  echo "  -o | --target-dir        : Target directory to which matching files should be moved, defaults to ~/.paperless"
  echo "  -p | --pattern           : Pattern used to match files in --source-dir, defaults to '*' (every file in --source-dir, except hidden files)"
  echo "  -? | -h | --help         : Display this help message"
  echo "   "
  echo "If source and target dirs have the same value, files obviously won't be moved to another folder."
  echo "However, they will be sequentially (re)named based on the script's starting date and time and the current process ID."
  echo "   "
}

function parse_args {
  pattern="*"
  # Match also hidden files
  # https://unix.stackexchange.com/questions/186214/how-to-match-with-hidden-files-inside-a-directory/186219
  # pattern="{,.[^.],..?}*"

  while [ "$1" != "" ]; do
    case "$1" in
      -i | --source-dir )     SOURCE_DIRECTORY="$2"; shift;;
      -o | --target-dir )     TARGET_DIRECTORY="$2"; shift;;
      -p | --pattern )        pattern="$2"; shift;;
      '-?' | -h | --help )    usage; exit 0;;
      * )                     echo "$1 isn't a valid argument"; usage; exit 1;;
    esac
    shift # move to next kv pair
  done

  # Check if --source-dir is a valid directory and current user has permissions to move/delete files
  # Use current working directory if --source-dir hasn't been specified
  if [ -z "$SOURCE_DIRECTORY" ]; then
    SOURCE_DIRECTORY=$(pwd)
  fi
  if [ ! -d "$SOURCE_DIRECTORY" ]; then
    echo "$SOURCE_DIRECTORY isn't a valid source directory. Make sure the directory exists and you have permissions to access it."
    exit 1
  fi
  if [ ! -w "$SOURCE_DIRECTORY" ]; then
    echo "$SOURCE_DIRECTORY isn't writable. Please review your permissions."
    exit 1
  fi

  # Likely the same for --target-dir
  if [ -z "$TARGET_DIRECTORY" ]; then
    # TODO Consider additional incoming and completed folder within target folder
    # TODO Consider additional user folders where completed documents can be fetched by the respective user (may be useful in a household with more than one person)
    mkdir -p ~/.paperless
    TARGET_DIRECTORY=~/.paperless
  fi
  if [ ! -d "$TARGET_DIRECTORY" ]; then
    echo "$TARGET_DIRECTORY isn't a valid target directory. Make sure the directory exists and you have permissions to access it."
    exit 1
  fi
  if [ ! -w "$TARGET_DIRECTORY" ]; then
    echo "$TARGET_DIRECTORY isn't writable. Please review your permissions."
    exit 1
  fi
}

function capture {
  # https://unix.stackexchange.com/questions/492366/iterating-over-files-in-bash-and-obtaining-the-index-and-the-counts/492367#492367
  # shellcheck disable=SC2206
  local files=(${SOURCE_DIRECTORY%/}/$pattern)
  local file_count=0

  for file in "${files[@]}"; do
    [ -e "$file" ] || continue
    [ ! -d "$file" ] || continue

    # Only process files modified at least $PAPERLESS_SH_CAPTURE_MIN_DOCUMENT_AGE seconds ago to make sure it's not currently being written
    # https://stackoverflow.com/questions/28337961/find-out-if-file-has-been-modified-within-the-last-2-minutes
    # https://stackoverflow.com/questions/394230/how-to-detect-the-os-from-a-bash-script
    local current_time=$(date +%s)
    if [ "$(uname)" == "Darwin" ] && ! stat --help | grep -q "coreutils"; then
      local file_time=$(stat -t %s -f %m "$file")
    else
      local file_time=$(stat -c %Y "$file")
    fi
    local time_diff=$((current_time - file_time))
    local min_document_age=${PAPERLESS_SH_CAPTURE_MIN_DOCUMENT_AGE:-300}
    # shellcheck disable=SC2086
    if [ $time_diff -lt $min_document_age ]; then
      echo "Skipping file $file as it doesn't meet the minimum document age requirement (PAPERLESS_SH_CAPTURE_MIN_DOCUMENT_AGE)."
      continue
    fi

    # https://stackoverflow.com/questions/965053/extract-filename-and-extension-in-bash
    local filename=$(basename -- "$file")
    case $(basename -- "$filename") in
      *.* )
        local extension="${filename##*.}"
        ;;
      * )
        unset extension
        ;;
    esac

    # Sequentially rename target file, include date and time the script has been started and current process ID in order to ensure unique filenames
    # Useful if a scanner doesn't keep track of the file name sequence but e.g. always starts with 'scan0001.pdf' when scanning to an empty network folder
    # https://stackoverflow.com/questions/8789729/how-to-zero-pad-a-sequence-of-integers-in-bash-so-that-all-have-the-same-width
    # https://stackoverflow.com/questions/2493642/how-does-a-linux-unix-bash-script-know-its-own-pid
    # https://unix.stackexchange.com/questions/248544/mv-move-file-only-if-destination-does-not-exist/248548
    local i=file_count
    while true; do
      i=$(( i + 1 ))

      printf -v j "%05d" $i
      if [ -z "$extension" ]; then
        local full_target_file_path="$TARGET_DIRECTORY/$START_DATETIME-$$-$j"
      else
        local full_target_file_path="$TARGET_DIRECTORY/$START_DATETIME-$$-$j.$extension"
      fi

      if [ ! -e "$full_target_file_path" ]; then
        mv "$file" "$full_target_file_path"
        file_count=$(( file_count + 1 ))
        break
      elif [ $j -gt 99999 ]; then
        echo "Maximum sequence reached. There's probably something wrong."
        exit 1
      fi
    done
  done

  if [ $file_count -gt 0 ]; then
    echo "Done. Captured $file_count file(s) to $TARGET_DIRECTORY."
  else
    echo "No file at $SOURCE_DIRECTORY matched the given pattern: $pattern"
  fi
}

function main {
  parse_args "$@"
  capture
}

main "$@";