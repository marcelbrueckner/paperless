# paperless.sh

Tasks like filing a tax return forces you to fight with a lot of documents. Automate that process as far as possible to have time for all the new shiny tech things.

## Document capture

At first, documents from different sources will be captured into a single target folder where processing takes place.

### Scan

I'm scanning my documents with an HP Color LaserJet MFP M477fdw. Files will be transferred from scanner to a network share on my Synology NAS. For this, the scanner uses a separate user account that has access to that specific network share (`smb://MY_NAS_IP/scan`) - nothing else.

An additional user (let's call it `paperless`) has access to that network share (more specifically, the respective folder on my NAS) that regularly grabs scanned documents and moves them to his own workspace.

See [`capture-files.sh`](capture/capture-files.sh)

As soon as the document has been digitized, it can basically be put into the trash. Exception may be made if it's one of the following document types:

* social security registration certificate
* Pension information
* Printout of the electronic income tax certificate (yes, Germany is weird - would rather like to have it sent to me as a PDF)
* notarized documents (since they're always stapled together and "littered" with stamps on the staple anyway, it is a good idea to ask the notary for a digital version of the document (fees may apply))
* Warranty documents, e.g. receipts and invoices for electronics. According to German law, you don't have to have a receipt as proof of purchase. A person who witnessed where you bought it is sufficient. Nevertheless, you may have some trouble with the cashier so you better keep it for your own interest.
* ...

### E-mail

* Fetch invoices attached to an e-mail or
* convert e-mails that represent the invoice itself (I'm looking at you, Apple AppStore invoice).

#### Convert invoices embedded in e-mails as HTML

On macOS, printing the e-mail via [MailMate](https://freron.com/) includes the mail's header on the page and there seems to be [no option to disable it](https://freron.lighthouseapp.com/projects/58672/tickets/792-print-without-header#ticket-792-17).

1. Save e-mail as `*.eml` by drag'n'droping desired mail (e.g. Apple's invoices with subject "Deine Rechnung von Apple") to `$TARGET_FOLDER`.
2. Remove everything outside and including the below boundaries (keeping only quoted-printable encoded HTML)

    ```plain
    # REMOVE EVERTHING ABOVE THE BOUNDARY BELOW (AND THE BOUNDARY ITSELF)

    ------=_Part_107492550_908912261.1498158304040
    Content-Type: text/html; charset=UTF-8
    Content-Transfer-Encoding: QUOTED-PRINTABLE
    Content-Disposition: inline

    # BUT KEEP HTML IN HERE

    ------=_Part_107492550_908912261.1498158304040--
    ```

3. Decode message to HTML with [qprint](http://www.fourmilab.ch/webtools/qprint/) (install via Homebrew)
    `qprint -d $TARGET_FOLDER/$MAIL_SUBJECT.eml $TARGET_FOLDER/$MAIL_SUBJECT.html`
4. Save as PDF via macOS print dialogue

### Online mailbox

Online mailboxes are popular with banks and insurance companies, but also Amazon and other online shops offer invoices for download instead of sending them via snail mail or e-mail.

* which tool?

### Other

Website to PDF conversion. The following list contains some tools that seem to be feasible at first sight. But there are an hundred more projects on GitHub. most of them can be discovered searching through tag `pdf`.

* [https://github.com/alvarcarto/url-to-pdf-api](https://github.com/alvarcarto/url-to-pdf-api)
* [https://github.com/danburzo/percollate](https://github.com/danburzo/percollate)
* [https://github.com/thecodingmachine/gotenberg](https://github.com/thecodingmachine/gotenberg)

## Processing

Once documents are captured in the `paperless` system-user's workspace, they get sorted and tagged via a bunch of tools. For example [Maid](https://github.com/benjaminoakes/maid) moves files based on their content and attributes. Similar alternative/complement may be [organize](https://github.com/tfeldmann/organize).

* Text recognition (OCRmyPDF) and deskewing for scanned documents
* Remove empty pages
  * [https://github.com/fritz-hh/OCRmyPDF/issues/98](https://github.com/fritz-hh/OCRmyPDF/issues/98)
* Tagging (realized via file naming)
* Move to target folder

## Further applications

`Just an idea:`
Search for all tax relevant documents, compare them against a list of mandatory documents and send a mail once they are all fetched. As my tax lady wants them to be printed (and sorted(!) - why don't I make my tax on my own?), I send all documents to [LetterXpress](https://www.letterxpress.de/briefe-uebertragen/api) where they get printed and sent (once I'm sure to have collected all relevant documents).
